package jcode.project.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * JSON工具类
 */
public class JSONUtils {

    private static final ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 将对象转换成json字符串
     *
     * @param o
     * @return
     */
    public static String toJSON(Object o) {
        try {
            return mapper.writeValueAsString(o);
        }catch (JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * 将对象转换成json字符串
     *
     * @param o
     * @return
     */
    public static String toPrettyJSON(Object o) {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
        }catch (JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }

    public static void printPrettyJSON(Object o) {
        try {
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o));
        }catch (JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * 将json字符串转换成对象
     *
     * @param json
     * @return
     */
    public static <T> T toObject(String json, Class<T> class1) {
        if(isNullOrEmpty(json)){
            return null;
        }

        try {
            return mapper.readValue(json, class1);
        }catch (JsonProcessingException e){
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> toList(String json, Class<T> class1) {
        try {
            return mapper.readValue(json, mapper.getTypeFactory().constructParametricType(ArrayList.class, class1));
        }catch (JsonProcessingException e){
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> List<T> toList(String json) {
        try {
            if(isNullOrEmpty(json)){
                return Collections.emptyList();
            }

            return mapper.readValue(json, List.class);
        }catch (JsonProcessingException e){
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T convertObject(Object object, Class<T> class1) {
        if(object == null){
            return null;
        }

        return toObject(JSONUtils.toJSON(object), class1);
    }

    public static <T> List<T> convertList(Object data, Class<T> dataClass) {
        if(data == null){
            return Collections.EMPTY_LIST;
        }

        return toList(JSONUtils.toJSON(data), dataClass);
    }

    public static boolean isNullOrEmpty(String toTest) {
        return toTest == null || toTest.length() == 0;
    }

}
